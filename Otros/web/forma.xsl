<?xml version="1.0" encoding="ISO-8859-15"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/root">
        <html>
            <head>
                <title>El tiempo</title>
                <LINK REL="StyleSheet" HREF="./css.css" TYPE="text/css"/>
                <link rel="icon" type="image/png" href="./imagenes/001lighticons-90.png"/>
            </head>
            <body>

                <div class="header">
                    <h2>El Tiempo en

                        <xsl:value-of select="nombre"></xsl:value-of>,

                        <xsl:value-of select="provincia"></xsl:value-of>
                    </h2>
                </div>

                <div class="recuadro">
                    <div class="hoy">
                        <div class="dia">
                            <xsl:apply-templates select="prediccion/dia [2]"/>
                        </div>
                        <div class="espacioicono">
                            <div class="icono">
                                <xsl:apply-templates select="prediccion/dia[2]/estado_cielo"/>
                            </div>
                        </div>
                        <div class="tempMaxMin">
                            <h3>T.Max:

                                <xsl:value-of select="prediccion/dia[2]/temperatura/maxima"/>�C<br/>T.Min:

                                <xsl:value-of select="prediccion/dia[2]/temperatura/minima"/>�C</h3>
                        </div>
                        <div class="realinfo">
                            <table class="tabla">
                                <tr>
                                    <th>
                                        Horas
                                    </th>

                                    <xsl:for-each select="prediccion/dia[2]/prob_precipitacion[(@periodo='00-06') or (@periodo='06-12') or (@periodo='12-18') or (@periodo='18-24')]">
                                        <td>
                                            <p><xsl:value-of select="@periodo"/>h</p>
                                        </td>
                                    </xsl:for-each>
                                </tr>
                                <tr>
                                    <th>
                                        Temperatura
                                    </th>

                                    <xsl:for-each select="prediccion/dia[2]/temperatura/dato">
                                        <td>
                                            <p><xsl:value-of select="."/>�C</p>
                                        </td>
                                    </xsl:for-each>
                                </tr>
                                <tr>
                                    <th>
                                        % Precipitaciones
                                    </th>

                                    <xsl:for-each select="prediccion/dia[2]/prob_precipitacion[(@periodo='00-06') or (@periodo='06-12') or (@periodo='12-18') or (@periodo='18-24')]">
                                        <td>
                                            <p><xsl:value-of select="."/>%</p>
                                        </td>
                                    </xsl:for-each>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div class="otros">
                        <div class="peque">
                            <div class="dia">
                                <xsl:apply-templates select="prediccion/dia [3]"/>
                            </div>
                            <div class="espacioicono">
                                <div class="icono">
                                    <xsl:apply-templates select="prediccion/dia[3]/estado_cielo"/>
                                </div>
                            </div>
                            <div class="tempMaxMinpeque">
                                <h3>T.Max:

                                    <xsl:value-of select="prediccion/dia[3]/temperatura/maxima"/>�C<br/>T.Min:

                                    <xsl:value-of select="prediccion/dia[3]/temperatura/minima"/>�C</h3>
                            </div>
                            <div class="realinfo">
                                <table class="tablapeque">
                                    <tr>
                                        <th>
                                            Hr:
                                        </th>

                                        <xsl:for-each select="prediccion/dia[3]/prob_precipitacion[(@periodo='00-12') or (@periodo='12-24')]">
                                            <td>
                                                <p><xsl:value-of select="@periodo"/>h</p>
                                            </td>
                                        </xsl:for-each>
                                    </tr>

                                    <!-- <tr>
                                        <th>
                                            Tmp:
                                        </th>

                                        <td>
                                            <p><xsl:value-of select="prediccion/dia[3]/temperatura/dato[1]"/>/<xsl:value-of select="prediccion/dia[3]/temperatura/dato[2]"/>�C</p>
                                        </td>
                                        <td>
                                            <p><xsl:value-of select="prediccion/dia[3]/temperatura/dato[3]"/>/<xsl:value-of select="prediccion/dia[3]/temperatura/dato[4]"/>�C</p>
                                        </td>
                                    </tr> -->

                                    <tr>
                                        <th>
                                            %Preci:
                                        </th>

                                        <xsl:for-each select="prediccion/dia[3]/prob_precipitacion[(@periodo='00-12') or (@periodo='12-24')]">
                                            <td>
                                                <p><xsl:value-of select="."/>%</p>
                                            </td>
                                        </xsl:for-each>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="peque">
                            <div class="dia">
                                <xsl:apply-templates select="prediccion/dia [4]"/>
                            </div>
                            <div class="espacioicono">
                                <div class="icono">
                                    <xsl:apply-templates select="prediccion/dia[4]/estado_cielo"/>
                                </div>
                            </div>
                            <div class="tempMaxMinpeque">
                                <h3>T.Max:

                                    <xsl:value-of select="prediccion/dia[4]/temperatura/maxima"/>�C<br/>T.Min:

                                    <xsl:value-of select="prediccion/dia[4]/temperatura/minima"/>�C</h3>
                            </div>
                            <div class="realinfo">
                                <table class="tablapeque">
                                    <tr>
                                        <th>
                                            Hr:
                                        </th>

                                        <xsl:for-each select="prediccion/dia[4]/prob_precipitacion[(@periodo='00-12') or (@periodo='12-24')]">
                                            <td>
                                                <p><xsl:value-of select="@periodo"/>h</p>
                                            </td>
                                        </xsl:for-each>
                                    </tr>

                                    <!-- <tr>
                                        <th>
                                            Tmp:
                                        </th>

                                        <td>
                                            <p><xsl:value-of select="prediccion/dia[4]/temperatura/dato[1]"/>/<xsl:value-of select="prediccion/dia[4]/temperatura/dato[2]"/>�C</p>
                                        </td>
                                        <td>
                                            <p><xsl:value-of select="prediccion/dia[4]/temperatura/dato[3]"/>/<xsl:value-of select="prediccion/dia[4]/temperatura/dato[4]"/>�C</p>
                                        </td>
                                    </tr> -->

                                    <tr>
                                        <th>
                                            %Preci:
                                        </th>

                                        <xsl:for-each select="prediccion/dia[4]/prob_precipitacion[(@periodo='00-12') or (@periodo='12-24')]">
                                            <td>
                                                <p><xsl:value-of select="."/>%</p>
                                            </td>
                                        </xsl:for-each>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="footer">
                    <p>XSL propiedad de Pablo Diaz Rubio.�</p>
                </div>
            </body>
        </html>
    </xsl:template>












    <!-- Template para sacar los iconos  -->
    <xsl:template match="prediccion/dia/estado_cielo">

        <xsl:variable name="desc" select="@descripcion"/>
        <xsl:if test="(@periodo='00-24')">

            <xsl:choose>
              <xsl:when test="@descripcion='Despejado'">
                  <img class="icono" title="{$desc}" src="./imagenes/001lighticons-02.png" alt="Despejado"/>
              </xsl:when >

              <xsl:when test="(@descripcion='Intervalos nubosos con nieve') or (@descripcion='Nuboso con nieve')
                 or (@descripcion='Muy nuboso con nieve')  or (@descripcion='Cubierto con nieve')">
                  <img class="icono" title="{$desc}" src="./imagenes/001lighticons-24.png" alt="Intervalos nubosos con nieve"/>
              </xsl:when >

              <xsl:when test="(@descripcion='Intervalos nubosos con tormenta') or (@descripcion='Nuboso con tormenta')
                 or (@descripcion='Muy nuboso con tormenta')  or (@descripcion='Cubierto con tormenta')">
                  <img class="icono" title="{$desc}" src="./imagenes/001lighticons-27.png" alt="Nuboso con tormenta"/>
              </xsl:when >

              <xsl:when test="(@descripcion=' Intervalos nubosos con tormenta y lluvia escasa') or (@descripcion='Nuboso con tormenta y lluvia escasa')
                 or (@descripcion='Muy nuboso con tormenta y lluvia escasa')  or (@descripcion='Cubierto con tormenta y lluvia escasa')">
                  <img class="icono" title="{$desc}" src="./imagenes/tormenta_lluvia_escasa.png" alt=" Intervalos nubosos con tormenta y lluvia escasa"/>
              </xsl:when >

              <xsl:when test="(@descripcion='Intervalos nubosos con nieve escasa') or (@descripcion='Nuboso con nieve escasa')
                 or (@descripcion='Muy nuboso con nieve escasa')  or (@descripcion='Cubierto con nieve escasa')">
                  <img class="icono" title="{$desc}" src="./imagenes/001lighticons-21.png" alt=" Intervalos nubosos con nieve escasa"/>
              </xsl:when >

                <xsl:when test="@descripcion='Intervalos nubosos'">
                    <img class="icono" title="{$desc}" src="./imagenes/001lighticons-08.png" alt="Intervalos nubosos"/>
                </xsl:when >

                <xsl:when test="@descripcion='Muy nuboso'">
                    <img class="icono" title="{$desc}" src="./imagenes/001lighticons-25.png" alt="Muy nuboso"/>
                </xsl:when >

                <xsl:when test="@descripcion='Muy nuboso con lluvia'">
                    <img class="icono" title="{$desc}" src="./imagenes/Muy nuboso con lluvia.png" alt="Muy nuboso con lluvia"/>
                </xsl:when >

                <xsl:when test="(@descripcion='Nuboso') or (@descripcion='Poco nuboso') or (@descripcion='Nubes altas')  or (@descripcion='Cubierto')">
                    <img class="icono" title="{$desc}" src="./imagenes/001lighticons-14.png" alt="Nuboso"/>
                </xsl:when >

                <xsl:when test="@descripcion='Cubierto con lluvia escasa'">
                    <img class="icono" title="{$desc}" src="./imagenes/001lighticons-17.png" alt="Cubierto con lluvia escasa"/>
                </xsl:when >

                <xsl:when test="@descripcion='Nuboso con lluvia'">
                    <img class="icono" title="{$desc}" src="./imagenes/001lighticons-18.png" alt="Nuboso con lluvia"/>
                </xsl:when >

                <xsl:when test="(@descripcion='Muy nuboso con lluvia escasa')  or (@descripcion='Nuboso con lluvia escasa')">
                    <img class="icono" title="{$desc}" src="./imagenes/001lighticons-34.png" alt="Muy nuboso con lluvia escasa"/>
                </xsl:when >

                <xsl:when test="(@descripcion='Intervalos nubosos con lluvia escasa') or (@descripcion='Intervalos nubosos con lluvia')">
                    <img class="icono" title="{$desc}" src="./imagenes/43.png" alt="Intervalos nubosos con lluvia escasa"/>
                </xsl:when >

                <xsl:when test="@descripcion='Cubierto con lluvia'">
                    <img class="icono" title="{$desc}" src="./imagenes/001lighticons-82.png" alt="Cubierto con lluvia"/>
                </xsl:when >

                <xsl:otherwise>
                    <img class="icono" title="{$desc}" src="./imagenes/001lighticons-92.png" alt="No disponible"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <!-- Template para sacar los dias  -->
    <xsl:template match="prediccion/dia  ">

        <xsl:value-of select="@fecha"/>
        <!--format-date(@fecha, '[F]') -->
    </xsl:template>

</xsl:stylesheet>
