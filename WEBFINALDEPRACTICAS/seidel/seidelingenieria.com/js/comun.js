
// Get IE or Edge browser version
var version = detectIE();

if (version === false) {
  /* document.getElementById('result').innerHTML = '<s>IE/Edge</s>'; */

} else if (version >= 12) {
  /* document.getElementById('result').innerHTML = 'Edge ' + version; */
  $('body').addClass('EDGE');
} else {
  /* document.getElementById('result').innerHTML = 'IE ' + version; */
  $('body').addClass('IE');
}

// add details to debug result
/* document.getElementById('details').innerHTML = window.navigator.userAgent; */

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
  var ua = window.navigator.userAgent;

  // Test values; Uncomment to check result …

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
  
  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
  
  // Edge 12 (Spartan)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
  
  // Edge 13
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}



//FIX PARA IMG ANIMADAS en Internet Explorer
if($("body").hasClass("IE")){
  $('#LogoSeidelAnimado').attr("src", "./img/LogoSeidel_trazadoRoto.svg");
  /*  $('#LogoSeidelAnimado').parent().addClass('row'); */
 /* $('#LogoSeidelAnimado').addClass('img-fluid');
  $('#LogoSeidelAnimado').removeClass('d-inline-block pull-left float-left fuenteCalibri align-top pr-2 d-flex align-items-center justify-content-center');
  $('#LogoSeidelAnimado').parent().find('p').removeClass('text-logo d-inline-block align-top d-flex align-items-center justify-content-center float-right pull-right py-0');*/
  $('#LogoSeidelAnimado1').attr("src", "../img/LogoSeidel_trazadoRoto.svg");
  $('#LogoSeidelAnimado2').attr("src", "../../img/LogoSeidel_trazadoRoto.svg"); 
}


//DETECTO CUANDO USAMOS SAFARI
/* function safari () {
  if(navigator.userAgent.toLowerCase().indexOf('safari/') > -1){
    $('body').addClass('safari');
  }
  
  if($("body").hasClass("safari")){
    
    $('#LogoSeidelAnimado').attr("src", "./img/LogoSeidel_trazadoRoto.svg");
    $('.row').addClass('MACrow');
    $('.row').removeClass('row');
   alert('Please view this webpage in a modern browser. \n Porfavor vea esta página en un navegador moderno.');
    $('html').css({'overflow':'scroll'});
    $('.informaticaCol').css({'min-height':'250px'});
    $('.marketingCol').css({'min-height':'250px'});
    $('.masthead').css({'padding-top':'300px'})
  }
  }
  safari(); */






// UNHIDES whole ELEMENT .hideme WHILE PASSING THROUGH
$(document).ready(function () {
  /* Every time the window is scrolled ... */
  $(window).scroll(function () {
    /* Check the location of each desired element */
    $('.hideme').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      /* If the object is completely visible in the window, fade it it */
      if (bottom_of_window > bottom_of_object) {
        $(this).animate({
          'opacity': '1'
        }, 1000);
      }
    });
  });
});

// UNHIDES child elements of .listaHideme WHILE PASSING THROUGH
$(document).ready(function () {
  $(window).scroll(function () {
    $('.listaHideme').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      var listItem = $(this).find('li');
      var childPlist = $(this).find('p');

      if (bottom_of_window > bottom_of_object) {
        if (listItem.length != 0) {
          var i;
          for (i = 0; i < listItem.length; i++) {
            $(listItem[i]).animate({
              'opacity': '1'
            }, (i + 1) * 1000);
          }
        } else if (childPlist.length != 0) {
          var i;
          for (i = 0; i < childPlist.length; i++) {
            $(childPlist[i]).animate({
              'opacity': '1'
            }, (i + 1) * 1000);
          }
        }
      }
    });
  });
});



//AUTOMATIC SCROLL DOWN
$(document).ready(function () {
  $('.slider').each(function (i) {
    $(this).animate({
      'opacity': '1'
    }, 1000);
    var top = ($(window).scrollTop() || $("body").scrollTop());
    if (top == 0) {
      setTimeout(
        function () {
          document.querySelector('#puntoStart').scrollIntoView({
            behavior: 'smooth'
          });
        }, 1000);
    }

  });
});

// UNHIDES whole ELEMENT '.carta' WHILE PASSING THROUGH
$(document).ready(function () {
  $('.carta').each(function (i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    if (bottom_of_window > bottom_of_object) {
      $(this).animate({
        'opacity': '1'
      }, 500);

    }

  });

});

//SLIDES DOWN the element .toscroll when scroll through .scrollme
var click;
click = false;
$('.toscroll').click(function () {
  click = true;
  if ($(".scrollme").is(":hidden")) {
    $(this).find(".scrollme").slideDown(1200).delay(800).fadeIn(400);
  }
  //   else {
  //    $( ".scrollme" ).hide();
  // }
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.toscroll').each(function (i) {
      var screenBottom = $(window).scrollTop() + $(window).height();
      var objectBottom = $(this).offset().top + $(this).outerHeight();
      if (screenBottom > objectBottom) {
        if (click == false) {

          /*  setTimeout(
             function () { */
          $(this).find(".scrollme").slideDown(800);
          /* },200); */

        }
      }
    });
  });
});


//LOADER
$(document).ready(function () {
  //En 1 segundo abrimos las secciones laterales
  /*   setTimeout(function(){
      $('.section-left').animate({
        left: "-=100%"   
      },2000);
      $('.section-right').animate({
        right: "-=100%"
      },2000);
    },2500);
   */
  //En 1.1 segundo quitamos la opacidad del logo
  setTimeout(function () {
    $('.loader-section').animate({
      'opacity': '0'
    }, 1200);

  }, 1500);
  var segundosEspera = 2500;
  if($("body").hasClass("IE")){
    segundosEspera = 0;
  }
  //En 1.5 segundos quitamos la seccion del DOM.
  setTimeout(function () {
    $('.loader-section').remove();
  }, segundosEspera); //2500 quitado para no perder tiempo

});

//Pausa el video cuando sales de la zona de header
$(document).ready(function () {
  
  $(window).scroll(function () {
    /* $('#video_background').get(0).play(); */
    $('#separador').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $('#video_background').get(0).pause();
      } else {
        $('#video_background').get(0).play();
      }
      if ($("#mainNav").offset().top == 0) {
        $('#video_background').get(0).play();
      }
    });
  });
});
/* var promise = document.querySelector('#video_background').play(); */

/* if (promise !== undefined) {
  promise.then(_ => { */
    // Autoplay started!
/*   }).catch(error => { */
    // Autoplay was prevented.
    // Show a "Play" button so that user can start playback.
/*     $('#video_background').get(0).play();
    console.log('VideoError');
  });
} */

/* Animación de las burbujas de consultoria. */
function speechBubbleAnswerActions(bubble) {
  if ($(bubble).find('p').hasClass('ingInf')) {
    $('.ingInf').parent().animate({
      'opacity': '1'
    }, 10);
    $('.ingInf').parent().addClass('animated flipInX');
    /*  $('.ingInf').attr("src", "../img/animated/5s/ConsultoriaIngenieriaInformatica_animated.svg"); */
  } else if ($(bubble).find('p').hasClass('sisInf')) {
    $('.sisInf').parent().animate({
      'opacity': '1'
    }, 10);
    $('.sisInf').parent().addClass('animated flipInX');
    /* $('.sisInf').attr("src", "../img/animated/5s/ConsultoriaSistemasInformaticos_animated.svg"); */
  } else if ($(bubble).find('p').hasClass('sftwr')) {
    $('.sftwr').parent().animate({
      'opacity': '1'
    }, 10);
    $('.sftwr').parent().addClass('animated flipInX');
    /* $('.sftwr').attr("src", "../img/animated/5s/ConsultoriaSoftware_animated.svg"); */
  } else if ($(bubble).find('p').hasClass('itStr')) {
    $('.itStr').parent().animate({
      'opacity': '1'
    }, 10);
    $('.itStr').parent().addClass('animated flipInX');
    /* $('.itStr').attr("src", "../img/animated/5s/ConsultoriaITStrategy_animated.svg"); */
  } else if ($(bubble).find('p').hasClass('mrktOnl')) {
    $('.mrktOnl').parent().animate({
      'opacity': '1'
    }, 10);
    $('.mrktOnl').parent().addClass('animated flipInX');
    /* $('.mrktOnl').attr("src", ""); */
  } else if ($(bubble).find('p').hasClass('eClou')) {
    $('.eClou').parent().animate({
      'opacity': '1'
    }, 10);
    $('.eClou').parent().addClass('animated flipInX');
    /* $('.eClou').attr("src", ""); */
  } else if ($(bubble).find('p').hasClass('segInf')) {
    $('.segInf').parent().animate({
      'opacity': '1'
    }, 10);
    $('.segInf').parent().addClass('animated flipInX');
    /* $('.segInf').attr("src", ""); */
  }
}
var animationQuestionEnd = (function (el) {
  var animations = {
    animation: 'animationend',
    OAnimation: 'oAnimationEnd',
    MozAnimation: 'mozAnimationEnd',
    WebkitAnimation: 'webkitAnimationEnd',
  };
  for (var t in animations) {
    if (el.style[t] !== undefined) {
      return animations[t];
    }
  }
})(document.createElement('div'));

function screenScrollBubble(bubble) {
  $(bubble).each(function (i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    if (bottom_of_window > bottom_of_object) {
      if ($(this).is('.speech-bubble-left')) {
        $(this).addClass('animated  slideInLeft');
      } else if ($(this).is('.speech-bubble-right')) {
        $(this).addClass('animated  slideInRight');
      }
      $(this).animate({
        'opacity': '1'
      }, 10);
      $(this).one(animationQuestionEnd, function () {
        if ($(this).is('.speech-bubble-left')) {
          var btemp = $(this).parent().parent().find(".speech-bubble-right-answer");
          speechBubbleAnswerActions(btemp);
        } else if ($(this).is('.speech-bubble-right')) {
          var btemp = $(this).parent().parent().find(".speech-bubble-left-answer");
          speechBubbleAnswerActions(btemp);
        }
      });
    }
  });
}
$(document).ready(function () {
  $(window).scroll(function () {
    var speechBubbleLeft = '.speech-bubble-left';
    var speechBubbleRight = '.speech-bubble-right';
    $('.bubbles').each(function (i) {
      if ($(this).is('.speech-bubble-left')) {
        screenScrollBubble(speechBubbleLeft);
      } else if ($(this).is('.speech-bubble-right')) {
        screenScrollBubble(speechBubbleRight);
      }
    });
  });
});



//ANIMATE.CSS
$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollBounceInDownAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated bounceInDown');
        $(this).removeClass('hidden');

      }
    });
  });
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollBounceInUpAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated bounceInUp');
        $(this).removeClass('hidden');

      }
    });
  });
});



function fzoomIn() {
  $('.scrollZoomInAnimated').each(function (i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    if (bottom_of_window > bottom_of_object) {
      $(this).addClass('animated zoomIn');
      $(this).removeClass('hidden');
    }
  });
}
var vzoomIn = fzoomIn();
$(document).ready(function () {
  fzoomIn();
  $(window).scroll(fzoomIn);
});

function ffadeInLeft() {
  $('.scrollfadeInLeftAnimated').each(function (i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    if (bottom_of_window > bottom_of_object) {
      $(this).addClass('animated fadeInLeft');
      $(this).removeClass('hidden');
    }
  });
}
var vfadeInLeft = ffadeInLeft();
$(document).ready(function () {
  ffadeInLeft();
  $(window).scroll(ffadeInLeft);
});

function ffadeInRight() {
  $('.scrollfadeInRightAnimated').each(function (i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    if (bottom_of_window > bottom_of_object) {
      $(this).addClass('animated fadeInRight');
      $(this).removeClass('hidden');
    }
  });
}
var vfadeInRight = ffadeInRight();
$(document).ready(function () {
  ffadeInRight();
  $(window).scroll(ffadeInRight);
});


function flightSpeedIn() {
  $('.scrolllightSpeedInAnimated').each(function (i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    if (bottom_of_window > bottom_of_object) {
      $(this).addClass('animated lightSpeedIn');
      $(this).removeClass('hidden');
    }
  });
}
var vlightSpeedIn = flightSpeedIn();
$(document).ready(function () {
  flightSpeedIn();
  $(window).scroll(flightSpeedIn);
});


$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollBounceInLeftAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated bounceInLeft');
        $(this).removeClass('hidden');
      }
    });
  });
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollBounceInRightAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated bounceInRight');
        $(this).removeClass('hidden');
      }
    });
  });
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollRubberAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated rubberBand');
      }
    });
  });
});


$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollPulseAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated pulse');
      }
    });
  });
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollFlashAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated flash');
      }
    });
  });
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollBounceAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated bounce');
      }
    });
  });
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollShakeAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated shake');
      }
    });
  });
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollJelloAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated jello');

      }
    });
  });
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.animateAttentionSeekers').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      var listItem = $(this).find('div');
      if (bottom_of_window > bottom_of_object) {
        if (listItem.length != 0) {
          var i;
          for (i = 0; i < listItem.length; i++) {
            if (!$(listItem[i]).hasClass('animated')) {
              var rNum = Math.floor((Math.random() * 3) + 1);
              if (rNum == 1) {
                $(listItem[i]).addClass('scrollPulseAnimated');
                $(listItem[i]).removeClass('hidden');
              } else if (rNum == 2) {
                $(listItem[i]).addClass('scrollFlashAnimated');
                $(listItem[i]).removeClass('hidden');
              } else if (rNum == 3) {
                $(listItem[i]).addClass('scrollBounceAnimated');
                $(listItem[i]).removeClass('hidden');
              }/*  else if (rNum == 4) {
                $(listItem[i]).addClass('scrollRubberAnimated');
                $(listItem[i]).removeClass('hidden');
              } else if (rNum == 5) {
                $(listItem[i]).addClass('scrollShakeAnimated');
                $(listItem[i]).removeClass('hidden');
              } else if (rNum == 6) {
                $(listItem[i]).addClass('scrollJelloAnimated');
                $(listItem[i]).removeClass('hidden'); 
              }*/
            }
          }
        }
      }
    });
  });
});
function fol() {
  $(this).addClass('animated  fadeOutLeft');
  $(this).animate({
    'background': '#fff'
  }, 500);

  $(this).removeClass('hidden');


}
var animationEnd = (function (el) {
  var animations = {
    animation: 'animationend',
    OAnimation: 'oAnimationEnd',
    MozAnimation: 'mozAnimationEnd',
    WebkitAnimation: 'webkitAnimationEnd',
  };

  for (var t in animations) {
    if (el.style[t] !== undefined) {
      return animations[t];
    }
  }
})(document.createElement('div'));





var animateBoxFadeInLeft = (function animateBoxFadeInLeft(el) {
  $('.halfScreenContainer').each(function (i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    /* var bottom_of_footer = $('.footer').offset().top + $(this).outerHeight(); */
    console.log('sdffsd');
    if (bottom_of_window > bottom_of_object) {
      fol();
      $(this).addClass('animated  fadeInLeft');
    } /* else if(bottom_of_window > bottom_of_footer){
      fol();
      $(this).addClass('animated  fadeInLeft');
    } */
  });
})

/* var animateBoxFadeInLeft = animateBoxFadeInLeft();
 */
$(document).ready(function () {
  animateBoxFadeInLeft();
  $(window).scroll(function(){
    
    animateBoxFadeInLeft();
  });
});



var animateBoxFadeInRight = (function animateBoxFadeInRight(el) {
  $('.halfScreenContainerRight').each(function (i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    /* var bottom_of_footer = $('.footer').offset().top + $(this).outerHeight(); */
    if (bottom_of_window > bottom_of_object) {
      fol();
      $(this).addClass('animated  fadeInRight');
      $(this).parent()
      .delay(1000)
      .queue(function (next) { 
        $(this).css('background', '#fff'); 
        next(); 
      });
    } 
  });
})
$(document).ready(function () {
  animateBoxFadeInRight();
  $(window).scroll(function(){
    animateBoxFadeInRight();
  });
});



var animateBoxFadeInRightINV = (function animateBoxFadeInRightINV(el) {
  $('.halfScreenContainerRightINV').each(function (i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    /* var bottom_of_footer = $('.footer').offset().top + $(this).outerHeight(); */
    if (bottom_of_window > bottom_of_object) {
      fol();
      $(this).addClass('animated  fadeInRight');
      $(this).parent()
      .delay(1000)
      .queue(function (next) { 
        $(this).css('background', '#003c52'); 
        next(); 
      });
    } 
  });
})
$(document).ready(function () {
  animateBoxFadeInRightINV();
  $(window).scroll(function(){
    animateBoxFadeInRightINV();
  });
});



$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollFlipInXAnimatedBox').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated flipInX');
        $(this).removeClass('hidden');

        $(this).parent()
      .delay(600)
      .queue(function (next) { 
        $(this).removeClass('bg-primary') ;
        next(); 
        $(this).addClass('bgIMG21'); 
        next(); 
      });

        $(this).parent().parent()
      .delay(600)
      .queue(function (next) { 
        $(this).removeClass('bg-primary') ;
        next(); 
        $(this).addClass('bgIMG21'); 
        next(); 
      });

      }
    });
  });
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.scrollFlipInXAnimated').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('animated flipInX');
        $(this).removeClass('hidden');

      }
    });
  });
});


//CAROUSEL Movimiento
$('#myCarousel').on('slide.bs.carousel', function (e) {
  var $e = $(e.relatedTarget);
  var idx = $e.index();
  var itemsPerSlide = 4;
  var totalItems = $('.carousel-item').length;
  
  if (idx >= totalItems-(itemsPerSlide-1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i=0; i<it; i++) {
          // append slides to end
          if (e.direction=="left") {
              $('.carousel-item').eq(i).appendTo('.carousel-inner');
          }
          else {
              $('.carousel-item').eq(0).appendTo('.carousel-inner');
          }
      }
  }
});
$('#myCarousel').carousel({ 
              interval: 2000
      });
$(document).ready(function() {
/* show lightbox when clicking a thumbnail */
  $('a.thumb').click(function(event){
    event.preventDefault();
    var content = $('.modal-body');
    content.empty();
      var title = $(this).attr("title");
      $('.modal-title').html(title);        
      content.html($(this).html());
      $(".modal-profile").modal({show:true});
  });
});
/* -------------------------------------------OBRAS----------------------------------------------- */

$(document).ready(function(){
  $('.bounceOnHover').each(function(i){
    
  $(this).hover(
    function() {
      
      $( this ).addClass( 'animated  bounce' );
    }, function() {
      $( this ).removeClass( 'animated  bounce' );
    }
  );
  });
});

//ANIMACION BOMBILLA BOTON CONTACTO
$(document).ready(function(){
  $('.bombillaBtn').each(function(i){
  $(this).hover(
    function() {
      $( this ).parent().append( $( "<span> CONTACTO</span>" ) );

      $(this).delay(40).queue(function (next) { 
        $(this).attr("src", "../../img/bombilla-on-software-medida-asturias.png");
        next(); 

      });
      $(this).delay(80).queue(function (next) { 
        $(this).attr("src", "../../img/bombilla-off-software-medida-asturias.png");
        next(); 
      });
      $(this).delay(50).queue(function (next) { 
        $(this).attr("src", "../../img/bombilla-on-software-medida-asturias.png");
        next(); 
      });
      $(this).delay(20).queue(function (next) { 
        $(this).attr("src", "../../img/bombilla-off-software-medida-asturias.png");
        next(); 
      });
      $(this).delay(90).queue(function (next) { 
        $(this).attr("src", "../../img/bombilla-on-software-medida-asturias.png");
        next(); 
      });
    }, function() {
      $(this).attr("src", "../../img/bombilla-off-software-medida-asturias.png");
      $( this ).parent().find( "span:last" ).remove();
    }
  );
  });
});

//ANIMACION AUTOESCRITURA PALABRAS ID
function animateLeters (elementId){
  var strEl = elementId.split("");
  var el = document.getElementById(elementId);
(function animate() {
  strEl.length > 0 ? el.innerHTML += strEl.shift() : clearTimeout(running); 
var running = setTimeout(animate, 90);
})();
}
var boolPru = false;
var boolFor = false;
var boolMan = false;
var boolInt = false;
$(document).ready(function () {
  $(window).scroll(function () {
    $('.ParCreLet').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        if(!boolPru){
          animateLeters('pruebas');
          boolPru = true;
        }
        if(!boolFor){
          animateLeters('formacion');
          boolFor = true;
        }
        if(!boolMan){
          animateLeters('mantenimiento');
          boolMan = true;
        }
        if(!boolInt){
          animateLeters('integracion');
          boolInt = true;
        }
      }
    });
  });
});

//PROGRESS BAR ANIMATION
$(document).ready(function () {
  $(window).scroll(function () {
    $('.progress-bar').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(this).animate({'opacity':'1'},100);
        $(this).addClass('active');
      }
    });
  });
});








