// UNHIDES whole ELEMENT .hideme WHILE PASSING THROUGH
$(document).ready(function () {
  /* Every time the window is scrolled ... */
  $(window).scroll(function () {
    /* Check the location of each desired element */
    $('.hideme').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      /* If the object is completely visible in the window, fade it it */
      if (bottom_of_window > bottom_of_object) {
        $(this).animate({
          'opacity': '1'
        }, 1000);
      }
    });
  });
});

// UNHIDES child elements of .listaHideme WHILE PASSING THROUGH
$(document).ready(function () {
  $(window).scroll(function () {
    $('.listaHideme').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      var listItem = $(this).find('li');
      var childPlist = $(this).find('p');

      if (bottom_of_window > bottom_of_object) {
        if (listItem.length != 0) {
          var i;
          for (i = 0; i < listItem.length; i++) {
            $(listItem[i]).animate({
              'opacity': '1'
            }, (i + 1) * 1000);
          }
        } else if (childPlist.length != 0) {
          var i;
          for (i = 0; i < childPlist.length; i++) {
            $(childPlist[i]).animate({
              'opacity': '1'
            }, (i + 1) * 1000);
          }
        }
      }
    });
  });
});



//AUTOMATIC SCROLL DOWN
$(document).ready(function () {
  $('.slider').each(function (i) {
    $(this).animate({
      'opacity': '1'
    }, 1000);
    var top = ($(window).scrollTop() || $("body").scrollTop());
    if (top == 0) {
      setTimeout(
        function () {
          document.querySelector('#puntoStart').scrollIntoView({
            behavior: 'smooth'
          });
        }, 1300);
    }

  });
});

// UNHIDES whole ELEMENT .hideme WHILE PASSING THROUGH
$(document).ready(function () {
  $('.carta').each(function (i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    if (bottom_of_window > bottom_of_object) {
      $(this).animate({
        'opacity': '1'
      }, 500);

    }

  });

});

//SLIDES DOWN the element .toscroll when scroll through .scrollme
var click;
click = false;
$('.toscroll').click(function () {
  click = true;
  if ($(".scrollme").is(":hidden")) {
    $(this).find(".scrollme").slideDown(1200).delay(800).fadeIn(400);
  }
  //   else {
  //    $( ".scrollme" ).hide();
  // }
});
$(document).ready(function () {
  $(window).scroll(function () {
    $('.toscroll').each(function (i) {
      var screenBottom = $(window).scrollTop() + $(window).height();
      var objectBottom = $(this).offset().top + $(this).outerHeight();
      if (screenBottom > objectBottom) {
        if (click == false) {
          
         /*  setTimeout(
            function () { */
              $(this).find(".scrollme").slideDown(800);
            /* },200); */

        }
      }
    });
  });
});


//LOADER
$(document).ready(function() {
  //En 1 segundo abrimos las secciones laterales
/*   setTimeout(function(){
    $('.section-left').animate({
      left: "-=100%"   
    },2000);
    $('.section-right').animate({
      right: "-=100%"
    },2000);
  },2500);
 */
//En 1.1 segundo quitamos la opacidad del logo
	setTimeout(function(){
    $('.loader-section').animate({
      'opacity': '0'
    }, 1200);

  }, 1500);
  
//En 1.5 segundos quitamos la seccion del DOM.
  setTimeout(function(){
    $('.loader-section').remove();
  },2500);
	
});

//Pausa el video cuando sales de la zona de header
$(document).ready(function () {
  $(window).scroll(function () {
    $('#separador').each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $('#video_background').get(0).pause();
      } else {
        $('#video_background').get(0).play();
      }
    });
  });
});